# Calculator
The calculator is a simple console application, which interprets a math expression, supporting variable assignment,
simple math functions, parentheses and more. 

### Notes
I implemented the execution algorithm, inspired by Dijkstra's shunting-yard algorithm.
However I decided to enable parentheses support and 
it is more complex to support unary operators.
The parsing is prone to edge cases but it is delicate, the text work there is quite basic. 
I'm sorry for not perfect readability and maybe not ideal design,but the lots of edge cases require somewhat of 
compromise and I can explaing everything I did or wrote. Hope to hear from you and I really hope you'll enjoy the assignment. 
