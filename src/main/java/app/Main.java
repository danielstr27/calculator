package app;

import app.exceptions.CalculatorException;
import app.interpreter.InFixInterpreter;
import app.interpreter.Interpreter;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Interpreter<String> interpreter = new InFixInterpreter();

        try {

            System.out.println("             _");
            System.out.println(" __ __ _____| |__ ___ _ __  ___ ");
            System.out.println(" \\ V  V / -_) / _/ _ \\ '  \\/ -_)");
            System.out.println("  \\_/\\_/\\___|_\\__\\___/_|_|_\\___|");
            while(true)
            {
                interpreter.evaluate(getConsoleInput());
                ((InFixInterpreter) interpreter).printMemory();
            }
        } catch (CalculatorException e) {
            e.printStackTrace();
            return;
        }
    }

    private static String getConsoleInput() {
        Scanner input = new Scanner(System.in);
        return input.nextLine();
    }
}
