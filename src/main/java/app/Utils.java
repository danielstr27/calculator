package app;

public class Utils {
    public static final int ULTIMATE_PRECEDENCE = 10;

    public static boolean isParentheses(char charAt) {
        return charAt == '(' || charAt == ')';
    }
}
