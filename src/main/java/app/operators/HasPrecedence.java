package app.operators;

public interface HasPrecedence {
    public int getPrecedence();
}
