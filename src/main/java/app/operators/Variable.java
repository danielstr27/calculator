package app.operators;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Variable implements Operand {
    private final String name;

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Variable))
            return false;

        return name.equals(((Variable) obj).name);
    }

    @Override
    public String toString() {
        return name;
    }
}
