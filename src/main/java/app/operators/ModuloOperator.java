package app.operators;

public class ModuloOperator implements BinaryOperator<Integer> {
    @Override
    public Integer apply(Integer first, Integer second) {
        return first % second;
    }

    @Override
    public int getPrecedence() {
        return 2;
    }
}