package app.operators.parentheses;

import app.operators.Token;

public class Open implements Token, Parentheses {
    @Override
    public int getPrecedence() {
        return 0;
    }
}
