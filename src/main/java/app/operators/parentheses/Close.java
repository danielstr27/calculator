package app.operators.parentheses;

import app.operators.Token;

public class Close implements Token, Parentheses {
    @Override
    public int getPrecedence() {
        return 0;
    }
}
