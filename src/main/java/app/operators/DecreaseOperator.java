package app.operators;

public class DecreaseOperator extends UnaryOperator<Integer> {

    public DecreaseOperator() {

    }

    public DecreaseOperator(boolean isPrefix) {
        super(isPrefix);
    }

    public Integer apply(Integer value) {
        return value - 1;
    }

    @Override
    public Integer apply(Integer first, Integer second) {
        return apply(first);
    }
}