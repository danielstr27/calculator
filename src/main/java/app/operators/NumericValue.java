package app.operators;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NumericValue<T> implements Operand {
    private T value;
}
