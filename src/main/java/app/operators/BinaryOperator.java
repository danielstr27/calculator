package app.operators;

public interface BinaryOperator<T> extends Operator, HasPrecedence {
    T apply(T first, T second);
}
