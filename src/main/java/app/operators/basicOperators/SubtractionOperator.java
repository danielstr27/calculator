package app.operators.basicOperators;

import app.operators.BinaryOperator;

public class SubtractionOperator implements BinaryOperator<Integer> {

    @Override
    public Integer apply(Integer first, Integer second) {
        return first + second;
    }

    @Override
    public int getPrecedence() {
        return 1;
    }
}
