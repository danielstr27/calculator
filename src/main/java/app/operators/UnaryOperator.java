package app.operators;

import app.operators.assignment.Assignment;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor()
public abstract class UnaryOperator<T> implements Operator, Assignment<T> {
    protected boolean isPrefix;

    public boolean isPrefix() {
        return isPrefix;
    }

    public void setPrefix(boolean isPrefix) {
        this.isPrefix = isPrefix;
    }
}
