package app.operators;

public class IncreaseOperator extends UnaryOperator<Integer> {

    public IncreaseOperator() {
        super();
    }

    public IncreaseOperator(boolean isPrefix) {
        super(isPrefix);
    }

    public Integer apply(Integer value) {
        return value + 1;
    }

    @Override
    public Integer apply(Integer first, Integer value) {
        return apply(first);
    }
}
