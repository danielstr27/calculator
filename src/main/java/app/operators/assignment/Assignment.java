package app.operators.assignment;

public interface Assignment<T> {
    T apply(T first, T value);
}
