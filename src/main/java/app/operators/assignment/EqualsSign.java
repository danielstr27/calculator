package app.operators.assignment;

import app.operators.Token;

public class EqualsSign implements Token, Assignment<Integer> {

    public Integer apply(Integer first, Integer second) {
        return second;
    }
}
