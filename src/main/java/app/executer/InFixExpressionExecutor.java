package app.executer;

import app.entities.Expression;
import app.exceptions.ExecutionException;
import app.exceptions.InvalidExpressionException;
import app.exceptions.UndeclaredVariableException;
import app.operators.*;
import app.operators.assignment.Assignment;
import app.operators.assignment.EqualsSign;
import app.operators.parentheses.Close;
import app.operators.parentheses.Open;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

@EqualsAndHashCode()
@Data
public class InFixExpressionExecutor implements Executor {
    public static final String SEPERATOR = "; ";
    public static final String EQUALS = "=";
    private Map<Variable, NumericValue<Integer>> variables;
    // Dijkstra shunting yard algorithm

    public InFixExpressionExecutor() {
        variables = new HashMap<>();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void execute(Expression expression) throws UndeclaredVariableException, InvalidExpressionException, ExecutionException {
        List<Token> expressionTokens = expression.getExpressionArray();
        boolean isAssignment = expressionTokens.get(1) instanceof Assignment && !(expressionTokens.get(1) instanceof UnaryOperator);
        Variable newVariable = null;
        Assignment<Integer> assignmentToken = null;

        if (isAssignment) {
            newVariable = ((Variable) expressionTokens.remove(0));
            assignmentToken = (Assignment<Integer>) expressionTokens.remove(0);
            if (!((assignmentToken instanceof EqualsSign) || variables.containsKey(newVariable))) {
                throw new UndeclaredVariableException("Invalid operator");
            }
        }

        NumericValue<Integer> result = evaluateExpression(expressionTokens, isAssignment);

        if (isAssignment) {
            if (variables.containsKey(newVariable))
                assign(newVariable, assignmentToken.apply(variables.get(newVariable).getValue(), result.getValue()));
            else
                assign(newVariable, assignmentToken.apply(0, result.getValue()));

        }
    }

    // Taking responsibility checking my casting
    @SuppressWarnings("unchecked")
    private NumericValue<Integer> evaluateExpression(List<Token> expressionTokens, boolean isAssignment) throws ExecutionException, InvalidExpressionException {
        Stack<Operand> valuesStack = new Stack<>();
        Stack<Token> operators = new Stack<>();

        // By nature of the stack, we can't hold the prefix in this stack, due to its higher
        // precedence. Hence, to ease our work, we hold the postfix, and prefix apart in different stacks
        Stack<Stack<Token>> unaryPostfixOperators = new Stack<>();
        Stack<UnaryOperator<Integer>> unaryPrefixOperators = new Stack<>();

        // create new "Scope" stack for postfix operators
        unaryPostfixOperators.add(new Stack<Token>());

        for (Token token : expressionTokens) {

            if (token instanceof NumericValue) {
                valuesStack.push((NumericValue<Integer>) token);

            } else if (token instanceof Variable) {
                Variable variable = ((Variable) token);
                if (!unaryPrefixOperators.isEmpty()) {
                    assignPrefixOperator(unaryPrefixOperators, variable);
                    unaryPrefixOperators.pop();
                }

                valuesStack.push(variable);

            } else if (token instanceof Open) {
                operators.push((Open) token);
                // create new "Scope" stack for postfix operators
                unaryPostfixOperators.add(new Stack<Token>());

            } else if (token instanceof Close) {
                while (!operators.isEmpty() && !(operators.peek() instanceof Open)) {
                    handleBinaryOperators(valuesStack, operators);
                }

                // The opening parentheses pop
                if (operators.isEmpty() || unaryPostfixOperators.isEmpty())
                    throw new InvalidExpressionException("Somehow, wrong parentheses expression was given");
                else {
                    operators.pop();
                    handlePostfixUnaryOperators(unaryPostfixOperators);
                    unaryPostfixOperators.pop();
                }

            } else if (token instanceof Operator) {
                if (token instanceof UnaryOperator) {
                    if (((UnaryOperator<Integer>) token).isPrefix()) {
                        unaryPrefixOperators.push((UnaryOperator<Integer>) token);
                    } else if (!unaryPostfixOperators.isEmpty() && unaryPostfixOperators.peek() != null) {
                        unaryPostfixOperators.peek().push(token);
                        unaryPostfixOperators.peek().push(valuesStack.peek());
                    } else {
                        throw new InvalidExpressionException("Invalid unary operators");
                    }
                    continue;
                }

                while (!operators.isEmpty() && (((HasPrecedence) operators.peek()).getPrecedence() >= ((HasPrecedence) token).getPrecedence())) {
                    handleBinaryOperators(valuesStack, operators);
                }

                operators.push((Operator) token);
            }
        }

        while (!operators.isEmpty()) {
            handleBinaryOperators(valuesStack, operators);
        }

        handlePostfixUnaryOperators(unaryPostfixOperators);

        if (valuesStack.peek() == null) {
            throw new ExecutionException("Final result isn't number");
        }

        return retrieveNumericValue(valuesStack.pop());
    }

    private void handleBinaryOperators(Stack<Operand> valuesStack, Stack<Token> operators) throws ExecutionException {
        BinaryOperator<Integer> operator = ((BinaryOperator<Integer>) operators.pop());
        Operand operand1 = (Operand) valuesStack.pop();
        Operand operand2 = (Operand) valuesStack.pop();

        NumericValue<Integer> value1 = retrieveNumericValue(operand1);
        NumericValue<Integer> value2 = retrieveNumericValue(operand2);

        Integer result = operator.apply(value1.getValue(), value2.getValue());
        valuesStack.push(new NumericValue<>(result));
    }

    private NumericValue<Integer> retrieveNumericValue(Operand operand1) {
        NumericValue<Integer> operandValue;
        if (operand1 instanceof Variable) {
            operandValue = variables.get(((Variable) operand1));
        } else {
            operandValue = (NumericValue<Integer>) operand1;
        }
        return operandValue;
    }

    private void handlePostfixUnaryOperators(Stack<Stack<Token>> unaryPostfixOperators) {
        while (!unaryPostfixOperators.peek().isEmpty()) {
            Variable variable = (Variable) unaryPostfixOperators.peek().pop();
            UnaryOperator<Integer> operator = (UnaryOperator<Integer>) unaryPostfixOperators.peek().pop();
            assign(variable, operator.apply(variables.get(variable).getValue(), 0));
        }
    }

    private void assignPrefixOperator(Stack<UnaryOperator<Integer>> unaryPrefixOperators, Variable variable) {
        UnaryOperator<Integer> operator = unaryPrefixOperators.peek();
        assign(variable, operator.apply(variables.get(variable).getValue(), 0));
    }

    private void assign(Variable variable, Integer value) {
        if (variables.containsKey(variable)) {
            variables.remove(variable);
        }
        variables.put(variable, new NumericValue<Integer>(value));
    }

    public void printMemory() {
        StringBuilder memoryString = new StringBuilder();

        for (Variable variable : variables.keySet()) {
            memoryString.append(variable.toString());
            memoryString.append(EQUALS);
            memoryString.append(variables.get(variable).getValue());
            memoryString.append(SEPERATOR);
        }

        if (memoryString.length() > 2) {
            memoryString.delete(memoryString.length() - 2, memoryString.length());
        }

        System.out.println(String.format("(%s)", memoryString.toString()));
    }
}
