package app.executer;

import app.entities.Expression;
import app.exceptions.ExecutionException;
import app.exceptions.InvalidExpressionException;
import app.exceptions.UndeclaredVariableException;

public interface Executor {
    void execute(Expression expression) throws UndeclaredVariableException, InvalidExpressionException, ExecutionException;

    void printMemory();
}
