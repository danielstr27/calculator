package app.entities;

import app.operators.Token;

import java.util.List;

public class InFixExpression extends Expression {
    public InFixExpression(List<Token> tokens) {
        super(tokens);
    }
}
