package app.entities;


import app.operators.Token;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public abstract class Expression {
    private List<Token> expressionArray;
}
