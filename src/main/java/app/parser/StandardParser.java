package app.parser;

import app.Utils;
import app.entities.InFixExpression;
import app.exceptions.*;
import app.factories.OperatorFactory;
import app.factories.SymbolFactory;
import app.operators.NumericValue;
import app.operators.Token;
import app.operators.Variable;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class StandardParser implements Parser<String> {

    @Override
    public InFixExpression parse(String expressionInput) throws ParseException {
        String cleanString = StringUtils.normalizeSpace(expressionInput);
        List<Token> tokens = new ArrayList<>();

        for (int index = 0; index < cleanString.length(); index++) {
            char currentChar = cleanString.charAt(index);
            if (Character.isSpaceChar(
                    currentChar)) {
                continue;
            }
            Token token;

            if (Character.isLetter(currentChar)) {
                String name = extractVariableName(index, cleanString);
                tokens.add(new Variable(name));
                index += name.length() - 1;
                continue;
            } else if (Character.isDigit(currentChar)) {
                String numericValue = extractNumericValue(index, cleanString);
                tokens.add(new NumericValue<>(Integer.parseInt(numericValue)));
                index += numericValue.length() - 1;
                continue;
            } else if (isOperator(index, cleanString)) {
                try {
                    String operatorString = operatorAt(index, cleanString);
                    tokens.add(OperatorFactory.getOperator(operatorString));
                    index += operatorString.length() - 1;
                } catch (UnknownOperatorException | OperatorCreationException exception) {
                    throw new ParseException(String.format("'%s' is unknown operator at %d", currentChar, index), exception);
                }
                continue;
            }

            try {
                String symbolString = symbolAt(index, cleanString);
                tokens.add(SymbolFactory.getSymbol(symbolString));
                index += symbolString.length() - 1;
            } catch (UnknownSymbolException exception) {
                throw new ParseException(String.format("'%s' is unknown symbol at %d", currentChar, index), exception);
            }
        }

        return new InFixExpression(tokens);
    }

    private boolean isOperator(int index, String cleanString) {
        return OperatorFactory.isOperator(extractText(index, cleanString));
    }

    private boolean isSymbol(int index, String cleanString) {
        return SymbolFactory.isSymbol(extractText(index, cleanString));
    }

    private String operatorAt(int index, String cleanString) {
        return extractText(index, cleanString);
    }

    private String symbolAt(int index, String cleanString) {
        return extractText(index, cleanString);
    }

    private String extractVariableName(int index, String cleanString) {
        int endOfSymbolIndex = index;
        while (endOfSymbolIndex < cleanString.length() &&
                (Character.isLetter(cleanString.charAt(endOfSymbolIndex))) &&
                !(Utils.isParentheses(cleanString.charAt(endOfSymbolIndex)) &&
                        !(Character.isSpaceChar(cleanString.charAt(endOfSymbolIndex))))) {

            endOfSymbolIndex++;
        }

        return cleanString.substring(index, endOfSymbolIndex);
    }

    private String extractNumericValue(int index, String cleanString) {
        int endOfSymbolIndex = index;
        while (endOfSymbolIndex < cleanString.length() &&
                (Character.isDigit(cleanString.charAt(endOfSymbolIndex))) &&
                !(Character.isSpaceChar(cleanString.charAt(endOfSymbolIndex)))) {
            endOfSymbolIndex++;
        }

        return cleanString.substring(index, endOfSymbolIndex);
    }

    private String extractText(int index, String cleanString) {
        int endOfSymbolIndex = index;
        boolean isOperatorTillLast = false;
        boolean isSymbolTillLast = false;

        while (endOfSymbolIndex < cleanString.length() &&
                !(Character.isDigit(cleanString.charAt(endOfSymbolIndex))) &&
                !(Character.isLetter(cleanString.charAt(endOfSymbolIndex))) &&
                !(Character.isSpaceChar(cleanString.charAt(endOfSymbolIndex)))) {

            if ((isOperatorTillLast && !OperatorFactory.isOperator(cleanString.substring(index, endOfSymbolIndex + 1)))
                    || (isSymbolTillLast && !SymbolFactory.isSymbol(cleanString.substring(index, endOfSymbolIndex + 1)))) {
                break;
            }

            isOperatorTillLast = OperatorFactory.isOperator(cleanString.substring(index, endOfSymbolIndex + 1));
            isSymbolTillLast = SymbolFactory.isSymbol(cleanString.substring(index, endOfSymbolIndex + 1));

            endOfSymbolIndex++;
        }

        return cleanString.substring(index, endOfSymbolIndex);
    }
}
