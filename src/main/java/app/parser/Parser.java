package app.parser;

import app.entities.Expression;
import app.exceptions.ParseException;

public interface Parser<T> {
    public Expression parse(T expressionInput) throws ParseException;
}
