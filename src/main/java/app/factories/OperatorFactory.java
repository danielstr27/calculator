package app.factories;

import app.exceptions.OperatorCreationException;
import app.exceptions.UnknownOperatorException;
import app.operators.DecreaseOperator;
import app.operators.IncreaseOperator;
import app.operators.ModuloOperator;
import app.operators.Operator;
import app.operators.basicOperators.AdditionOperator;
import app.operators.basicOperators.DivisionOperator;
import app.operators.basicOperators.MultiplicationOperator;
import app.operators.basicOperators.SubtractionOperator;

import java.util.Map;
import java.util.function.Supplier;

public class OperatorFactory {
    private static final Map<String, Supplier<Operator>> operatorMap = Map.of(
            "+", AdditionOperator::new,
            "-", SubtractionOperator::new,
            "*", MultiplicationOperator::new,
            "/", DivisionOperator::new,
            "%", ModuloOperator::new,
            "++", IncreaseOperator::new,
            "--", DecreaseOperator::new
    );

    public static Operator getOperator(String symbol) throws UnknownOperatorException, OperatorCreationException {
        Supplier<Operator> operatorSupplier = operatorMap.get(symbol);

        if (operatorSupplier == null) {
            throw new UnknownOperatorException(String.format("%s is unknown math operation", symbol));
        }

        return operatorSupplier.get();
    }

    public static boolean isOperator(String symbol) {
        return operatorMap.containsKey(symbol);
    }
}
