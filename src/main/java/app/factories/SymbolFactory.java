package app.factories;

import app.exceptions.UnknownSymbolException;
import app.operators.Token;
import app.operators.assignment.EqualsSign;
import app.operators.parentheses.Close;
import app.operators.parentheses.Open;

import java.util.Map;
import java.util.function.Supplier;

public class SymbolFactory {
    private static final Map<String, Supplier<Token>> operatorMap = Map.of(
            "=", EqualsSign::new,
            "(", Open::new,
            ")", Close::new
    );

    public static Token getSymbol(String symbol) throws UnknownSymbolException {
        Supplier<Token> operatorSupplier = operatorMap.get(symbol);

        if (operatorSupplier == null) {
            throw new UnknownSymbolException(String.format("%s is unknown math operation", symbol));
        }

        return operatorSupplier.get();
    }

    public static boolean isSymbol(String symbol) {
        return operatorMap.containsKey(symbol);
    }
}
