package app.exceptions;

public class UnknownOperatorException extends Throwable {
    public UnknownOperatorException(String message) {
        super(message);
    }
}
