package app.exceptions;

public class InvalidExpressionException extends Throwable {
    public InvalidExpressionException(String message) {
        super(message);
    }
}
