package app.exceptions;

public class OperatorCreationException extends Throwable {
    public OperatorCreationException(String message, Throwable exception) {
        super(message, exception);
    }
}
