package app.exceptions;

public class UndeclaredVariableException extends Throwable {
    public UndeclaredVariableException(String message) {
        super(message);
    }
}
