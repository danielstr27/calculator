package app.exceptions;

public class
SymbolCreationException extends Throwable {
    public SymbolCreationException(String message) {
        super(message);
    }

    public SymbolCreationException(String s, Throwable exception) {
        super(s, exception);
    }
}
