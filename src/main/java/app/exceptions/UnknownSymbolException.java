package app.exceptions;

public class UnknownSymbolException extends Exception {
    public UnknownSymbolException(String message) {
        super(message);
    }
}
