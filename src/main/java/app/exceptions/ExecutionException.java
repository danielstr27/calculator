package app.exceptions;

public class ExecutionException extends Throwable {
    public ExecutionException(String message) {
        super(message);
    }
}
