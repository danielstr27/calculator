package app.exceptions;

public class CalculatorException extends Throwable {
    public CalculatorException(String message) {
        super(message);
    }

    public CalculatorException(String message, Throwable exception) {
        super(message, exception);
    }
}
