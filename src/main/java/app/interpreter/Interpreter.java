package app.interpreter;

import app.exceptions.CalculatorException;
import app.executer.Executor;
import app.parser.Parser;
import app.validator.ExpressionValidator;

public abstract class Interpreter<T> {
    protected Parser<T> parser;
    protected ExpressionValidator validator;
    protected Executor executor;

    public abstract void evaluate(T input) throws CalculatorException;
}
