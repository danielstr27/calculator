package app.interpreter;

import app.entities.Expression;
import app.exceptions.*;
import app.executer.InFixExpressionExecutor;
import app.parser.StandardParser;
import app.validator.InFixExpressionValidator;

public class InFixInterpreter extends Interpreter<String> {

    public InFixInterpreter() {
        this.parser = new StandardParser();
        this.validator = new InFixExpressionValidator();
        this.executor = new InFixExpressionExecutor();
    }

    @Override
    public void evaluate(String input) throws CalculatorException {
        try {
            Expression expression = parser.parse(input);
            if (validator.validate(expression)) {
                executor.execute(expression);
            } else {
                throw new CalculatorException(String.format("Evaluation of '%s' failed", input));
            }
        } catch (ParseException | InvalidExpressionException | UndeclaredVariableException | ExecutionException exception) {
            throw new CalculatorException(String.format("Evaluation of '%s' failed", input), exception);
        }
    }

    public void printMemory() {
        executor.printMemory();
    }
}
