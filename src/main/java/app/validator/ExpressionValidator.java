package app.validator;

import app.entities.Expression;
import app.exceptions.InvalidExpressionException;

public interface ExpressionValidator {
    public boolean validate(Expression expression) throws InvalidExpressionException;
}
