package app.validator;

import app.entities.Expression;
import app.exceptions.InvalidExpressionException;
import app.operators.*;
import app.operators.assignment.Assignment;
import app.operators.parentheses.Close;
import app.operators.parentheses.Open;
import app.operators.parentheses.Parentheses;

import java.util.ArrayList;
import java.util.List;

public class InFixExpressionValidator implements ExpressionValidator {
    @Override
    public boolean validate(Expression expression) throws InvalidExpressionException {
        List<Token> expressionArray = expression.getExpressionArray();
        List<Token> expressionTokens = new ArrayList<>(expressionArray.subList(0, expressionArray.size()));

        boolean isAssignment = expressionTokens.get(1) instanceof Assignment;
        int assignments = 0;
        int parantheses = 0;

        if (isAssignment) {
            if (!(expressionTokens.get(0) instanceof Variable)) {
                throw new InvalidExpressionException("Assignment not to variable");
            }

            expressionTokens.remove(0);
            expressionTokens.remove(0);
        }

        if (!validateUnary(expressionTokens))
            throw new InvalidExpressionException("Invalid expression at unary operator");

        for (int index = 0; index < expressionTokens.size(); index++) {
            Token currentToken = expressionTokens.get(index);

            if (currentToken instanceof UnaryOperator)
                continue;

            assignments = assign(assignments, currentToken);
            parantheses = updateParenthesesCount(parantheses, currentToken);

            if (firstElement(index)) {
                if (!(currentToken instanceof Operand || currentToken instanceof Open)) {
                    throw new InvalidExpressionException("Expression starts with symbol, not var assignment/number/parentheses.");
                }
            } else {
                if (lastElement(expressionTokens, index) && !(currentToken instanceof Operand || currentToken instanceof Close)) {
                    throw new InvalidExpressionException("Expression ends with symbol, not var assignment/ number/parentheses.");
                }

                if (!validateSequence(expressionTokens.get(index - 1), currentToken)) {
                    throw new InvalidExpressionException(String.format("Invalid expression at token: %s", currentToken));
                }
            }
        }

        validateParentheses(parantheses);

        validateAssignments(assignments);

        if (isAssignment) {
            expressionTokens.add(0, expressionArray.get(1));
            expressionTokens.add(0, expressionArray.get(0));
        }

        expression.setExpressionArray(expressionTokens);

        return true;
    }

    @SuppressWarnings("unchecked")
    private boolean validateUnary(List<Token> expressionTokens) {
        for (int index = 0; index < expressionTokens.size(); index++) {
            Token token = expressionTokens.get(index);
            if (token instanceof UnaryOperator) {
                if (firstElement(index)) {
                    if (!(expressionTokens.get(1) instanceof Variable))
                        return false;
                    ((UnaryOperator<Integer>) token).setPrefix(true);
                    continue;
                } else if (lastElement(expressionTokens, index)) {
                    if (!(expressionTokens.get(expressionTokens.size() - 2) instanceof Variable))
                        return false;
                    ((UnaryOperator<Integer>) token).setPrefix(false);
                    continue;
                }

                if ((expressionTokens.get(index - 1) instanceof Variable)) {
                    ((UnaryOperator<Integer>) token).setPrefix(false);
                } else if ((expressionTokens.get(index + 1) instanceof Variable)) {
                    ((UnaryOperator<Integer>) token).setPrefix(true);
                } else return false;
            }
        }

        return true;
    }

    private boolean lastElement(List<Token> expressionTokens, int index) {
        return index == (expressionTokens.size() - 1);
    }

    private boolean firstElement(int index) {
        return index == 0;
    }

    private int updateParenthesesCount(int parentheses, Token token) throws InvalidExpressionException {
        if (token instanceof Open) {
            parentheses++;
        } else if (token instanceof Close) {
            parentheses--;
        }

        if (parentheses < 0) {
            throw new InvalidExpressionException("Parentheses invalid in their order");
        }

        return parentheses;
    }

    private void validateParentheses(int parentheses) throws InvalidExpressionException {
        if (parentheses != 0) {
            throw new InvalidExpressionException("Parentheses invalid in their order");
        }
    }

    private void validateAssignments(int assignments) throws InvalidExpressionException {
        if (assignments != 0) {
            throw new InvalidExpressionException("Assignments invalid in their order");
        }
    }

    private int assign(int assignments, Token token) throws InvalidExpressionException {
        if (token instanceof Assignment) {
            if (assignments >= 1) {
                throw new InvalidExpressionException("Valid expression cannot assign twice");
            }

            return assignments + 1;
        }
        return assignments;
    }

    private boolean validateSequence(Token lastToken, Token currentToken) {
        if (currentToken instanceof Open &&
                !(lastToken instanceof Operator || lastToken instanceof Open)) {
            return false;
        }

        if (currentToken instanceof Close &&
                !(lastToken instanceof Operand || lastToken instanceof Parentheses || lastToken instanceof UnaryOperator)) {
            return false;
        }

        if (currentToken instanceof BinaryOperator &&
                !(lastToken instanceof NumericValue || lastToken instanceof Variable || lastToken instanceof Close || lastToken instanceof UnaryOperator)) {
            return false;
        }

        if (currentToken instanceof Variable &&
                ((lastToken instanceof Operand || lastToken instanceof Close))) {
            return false;
        }

        if (currentToken instanceof NumericValue &&
                ((lastToken instanceof Operand || lastToken instanceof Close))) {
            return false;
        }

        return true;
    }
}
