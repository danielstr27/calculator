package integration;

import app.exceptions.CalculatorException;
import app.interpreter.InFixInterpreter;
import app.interpreter.Interpreter;
import org.junit.jupiter.api.Test;

public class InterpreterTest {

    @Test
    public void assignmentExampleTest() throws CalculatorException {
        Interpreter<String> interpreter = new InFixInterpreter();

        interpreter.evaluate("i=0");
        interpreter.evaluate("j = ++i");
        interpreter.evaluate("x = i++ + 5");
        interpreter.evaluate("y = 5 + 3 * 10");
        interpreter.evaluate("i += y");

        ((InFixInterpreter) interpreter).printMemory();
    }

    @Test
    public void test2() throws CalculatorException {
        Interpreter<String> interpreter = new InFixInterpreter();

        interpreter.evaluate("i=1");
        interpreter.evaluate("x = i++ + 5");
        interpreter.evaluate("y = 5 + 3 * 10");
        interpreter.evaluate("z = (y++ + x) * 10 * (5 + x++)");

        ((InFixInterpreter) interpreter).printMemory();
    }

    @Test
    public void test3() throws CalculatorException {
        Interpreter<String> interpreter = new InFixInterpreter();

        interpreter.evaluate("i=1");
        interpreter.evaluate("x = i++ + 5");
        interpreter.evaluate("y = 5 + 3 * 10");
        interpreter.evaluate("z = (y++ + ++x) * 10 * (5 + x)");

        ((InFixInterpreter) interpreter).printMemory();
    }
}
