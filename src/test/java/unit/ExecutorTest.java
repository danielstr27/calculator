package unit;

import app.entities.Expression;
import app.entities.InFixExpression;
import app.exceptions.ExecutionException;
import app.exceptions.InvalidExpressionException;
import app.exceptions.UndeclaredVariableException;
import app.executer.Executor;
import app.executer.InFixExpressionExecutor;
import app.operators.IncreaseOperator;
import app.operators.NumericValue;
import app.operators.Token;
import app.operators.Variable;
import app.operators.assignment.AdditionAssignmentOperator;
import app.operators.assignment.EqualsSign;
import app.operators.basicOperators.AdditionOperator;
import app.operators.basicOperators.MultiplicationOperator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ExecutorTest {

    @Test
    public void givenValidExpression() {
        Executor executor = new InFixExpressionExecutor();
        List<Token> tokens1 = new ArrayList<>();
        List<Token> tokens2 = new ArrayList<>();
        Variable a = new Variable("a");
        tokens1.add(a);
        tokens1.add(new EqualsSign());
        tokens1.add(new NumericValue<>(1));

        tokens2.add(new Variable("b"));
        tokens2.add(new EqualsSign());
        tokens2.add(a);
        tokens2.add(new AdditionOperator());
        tokens2.add(new NumericValue<>(5));
        Expression expression1 = new InFixExpression(tokens1);
        Expression expression2 = new InFixExpression(tokens2);
        Assertions.assertDoesNotThrow(() -> {
            executor.execute(expression1);
        });
        Assertions.assertDoesNotThrow(() -> {
            executor.execute(expression2);
        });

        executor.printMemory();
    }

    @Test
    public void givenParenthesesValidExpression() throws UndeclaredVariableException, InvalidExpressionException, ExecutionException {
        Executor executor = new InFixExpressionExecutor();
        List<Token> tokens1 = new ArrayList<>();
        List<Token> tokens2 = new ArrayList<>();
        Variable a = new Variable("a");
        tokens1.add(a);
        tokens1.add(new EqualsSign());
        tokens1.add(new NumericValue<>(1));

        tokens2.add(new Variable("b"));
        tokens2.add(new EqualsSign());
        tokens2.add(a);
        tokens2.add(new IncreaseOperator());
        tokens2.add(new AdditionOperator());
        tokens2.add(new NumericValue<>(5));
        Expression expression1 = new InFixExpression(tokens1);
        Expression expression2 = new InFixExpression(tokens2);
        Assertions.assertDoesNotThrow(() -> {
            executor.execute(expression1);
        });
        Assertions.assertDoesNotThrow(() -> {
            executor.execute(expression2);
        });

        executor.printMemory();
    }

    @Test
    public void assignmentExampleTest() {
        Executor executor = new InFixExpressionExecutor();
        List<Token> tokens1 = new ArrayList<>();
        List<Token> tokens2 = new ArrayList<>();
        List<Token> tokens3 = new ArrayList<>();
        List<Token> tokens4 = new ArrayList<>();
        List<Token> tokens5 = new ArrayList<>();
        Variable i = new Variable("i");
        Variable j = new Variable("j");
        Variable x = new Variable("x");
        Variable y = new Variable("y");

        tokens1.add(i);
        tokens1.add(new EqualsSign());
        tokens1.add(new NumericValue<>(0));

        tokens2.add(j);
        tokens2.add(new EqualsSign());
        tokens2.add(new IncreaseOperator(true));
        tokens2.add(i);

        tokens3.add(x);
        tokens3.add(new EqualsSign());
        tokens3.add(i);
        tokens3.add(new IncreaseOperator());
        tokens3.add(new AdditionOperator());
        tokens3.add(new NumericValue<>(5));

        tokens4.add(y);
        tokens4.add(new EqualsSign());
        tokens4.add(new NumericValue<>(5));
        tokens4.add(new AdditionOperator());
        tokens4.add(new NumericValue<>(3));
        tokens4.add(new MultiplicationOperator());
        tokens4.add(new NumericValue<>(10));

        tokens5.add(i);
        tokens5.add(new AdditionAssignmentOperator());
        tokens5.add(y);

        Expression expression1 = new InFixExpression(tokens1);
        Expression expression2 = new InFixExpression(tokens2);
        Expression expression3 = new InFixExpression(tokens3);
        Expression expression4 = new InFixExpression(tokens4);
        Expression expression5 = new InFixExpression(tokens5);
        Assertions.assertDoesNotThrow(() -> {
            executor.execute(expression1);
        });
        Assertions.assertDoesNotThrow(() -> {
            executor.execute(expression2);
        });

        Assertions.assertDoesNotThrow(() -> {
            executor.execute(expression3);
        });
        Assertions.assertDoesNotThrow(() -> {
            executor.execute(expression4);
        });

        Assertions.assertDoesNotThrow(() -> {
            executor.execute(expression5);
        });

        executor.printMemory();
    }
}