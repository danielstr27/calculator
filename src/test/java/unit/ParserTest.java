package unit;

import app.entities.Expression;
import app.entities.InFixExpression;
import app.exceptions.ParseException;
import app.operators.IncreaseOperator;
import app.operators.NumericValue;
import app.operators.Token;
import app.operators.Variable;
import app.operators.assignment.EqualsSign;
import app.operators.basicOperators.AdditionOperator;
import app.operators.basicOperators.MultiplicationOperator;
import app.operators.parentheses.Close;
import app.operators.parentheses.Open;
import app.parser.Parser;
import app.parser.StandardParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ParserTest {
    @Test
    public void givenInvalidSymbolInExpressionExceptionThrown() {
        Parser<String> parser = new StandardParser();
        Assertions.assertThrows(ParseException.class, () -> {
            parser.parse("a=2!@#");
        });
    }

    @Test
    public void givenAllValidSymbolsInExpressionThenReturnIt() throws ParseException {
        Parser<String> parser = new StandardParser();
        Assertions.assertNotNull(parser.parse("a = i + 2 * (3)"));
    }

    @Test
    public void givenBigSpacesThenExpressionReturned() throws ParseException {
        Parser<String> parser = new StandardParser();
        List<Token> expressionArray = parser.parse("a    =    i   +   2   *  (   3    )         ").getExpressionArray();
        Assertions.assertTrue(expressionArray.get(0) instanceof Variable);
        Assertions.assertTrue(expressionArray.get(1) instanceof EqualsSign);
        Assertions.assertTrue(expressionArray.get(2) instanceof Variable);
        Assertions.assertTrue(expressionArray.get(3) instanceof AdditionOperator);
        Assertions.assertTrue(expressionArray.get(4) instanceof NumericValue);
        Assertions.assertTrue(expressionArray.get(5) instanceof MultiplicationOperator);
        Assertions.assertTrue(expressionArray.get(6) instanceof Open);
        Assertions.assertTrue(expressionArray.get(7) instanceof NumericValue);
        Assertions.assertTrue(expressionArray.get(8) instanceof Close);
    }

    @Test
    public void givenExpressionWithPostfixUnaryOperatorExpressionReturned() throws ParseException {
        Parser<String> parser = new StandardParser();
        List<Token> expressionArray = parser.parse("a = i++ + 2 * ( 3 )").getExpressionArray();
        Assertions.assertTrue(expressionArray.get(0) instanceof Variable);
        Assertions.assertTrue(expressionArray.get(1) instanceof EqualsSign);
        Assertions.assertTrue(expressionArray.get(2) instanceof Variable);
        Assertions.assertTrue(expressionArray.get(3) instanceof IncreaseOperator);
        Assertions.assertTrue(expressionArray.get(4) instanceof AdditionOperator);
        Assertions.assertTrue(expressionArray.get(5) instanceof NumericValue);
        Assertions.assertTrue(expressionArray.get(6) instanceof MultiplicationOperator);
        Assertions.assertTrue(expressionArray.get(7) instanceof Open);
        Assertions.assertTrue(expressionArray.get(8) instanceof NumericValue);
        Assertions.assertTrue(expressionArray.get(9) instanceof Close);
    }

    @Test
    public void givenExpressionWithPrefixUnaryOperatorExpressionReturned() throws ParseException {
        Parser<String> parser = new StandardParser();
        List<Token> expressionArray = parser.parse("a = ++i + 2 * ( 3 )").getExpressionArray();
        Assertions.assertTrue(expressionArray.get(0) instanceof Variable);
        Assertions.assertTrue(expressionArray.get(1) instanceof EqualsSign);
        Assertions.assertTrue(expressionArray.get(2) instanceof IncreaseOperator);
        Assertions.assertTrue(expressionArray.get(3) instanceof Variable);
        Assertions.assertTrue(expressionArray.get(4) instanceof AdditionOperator);
        Assertions.assertTrue(expressionArray.get(5) instanceof NumericValue);
        Assertions.assertTrue(expressionArray.get(6) instanceof MultiplicationOperator);
        Assertions.assertTrue(expressionArray.get(7) instanceof Open);
        Assertions.assertTrue(expressionArray.get(8) instanceof NumericValue);
        Assertions.assertTrue(expressionArray.get(9) instanceof Close);
    }

    private Expression expression() {
        List<Token> tokens = new ArrayList<>();

        return new InFixExpression(tokens);
    }
}
