package unit;

import app.entities.Expression;
import app.entities.InFixExpression;
import app.exceptions.ExecutionException;
import app.exceptions.InvalidExpressionException;
import app.exceptions.UndeclaredVariableException;
import app.operators.IncreaseOperator;
import app.operators.NumericValue;
import app.operators.Token;
import app.operators.Variable;
import app.operators.assignment.EqualsSign;
import app.operators.basicOperators.AdditionOperator;
import app.operators.basicOperators.MultiplicationOperator;
import app.operators.parentheses.Close;
import app.operators.parentheses.Open;
import app.validator.ExpressionValidator;
import app.validator.InFixExpressionValidator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static unit.Utils.*;

public class ValidatorTest {
    @Test
    public void givenInvalidExpressionWithDoubleOperatorsThrowsException() {
        ExpressionValidator validator = new InFixExpressionValidator();
        Assertions.assertThrows(InvalidExpressionException.class, () -> validator.validate(buildInvalidDoubleOperatorsExpression()));
    }

    @Test
    public void givenValidVariablesAdditionExpression() throws InvalidExpressionException {
        ExpressionValidator validator = new InFixExpressionValidator();
        Assertions.assertTrue(validator.validate(buildValidVariablesAdditionExpression()));
    }

    @Test
    public void givenTwoOrderedParenthesesReturnsTrue() throws InvalidExpressionException {
        ExpressionValidator validator = new InFixExpressionValidator();
        Assertions.assertTrue(validator.validate(a()));
    }

    @Test
    public void givenTwoSwappedParenthesesExceptionThrown() {
        ExpressionValidator validator = new InFixExpressionValidator();
        Assertions.assertThrows(InvalidExpressionException.class, () -> {
            validator.validate(b());
        });
    }

    @Test
    public void givenExpressionWithUnaryOperatorAtEndThenReturnsTrue() throws InvalidExpressionException {
        ExpressionValidator validator = new InFixExpressionValidator();
        Assertions.assertTrue(validator.validate(c()));
    }

    @Test
    public void givenExpressionWithMoreThanOneAssignmentThenExceptionThrown() {
        ExpressionValidator validator = new InFixExpressionValidator();
        Assertions.assertThrows(InvalidExpressionException.class, () -> validator.validate(d()));
    }

    @Test
    public void givenExpressionWithTwoAdjacentOperatorsExceptionThrown() throws InvalidExpressionException {
        ExpressionValidator validator = new InFixExpressionValidator();
        Assertions.assertThrows(InvalidExpressionException.class, () -> validator.validate(e()));
    }

    @Test
    public void givenExpressionWithTwoAdjacentUnaryOperatorsExceptionThrown() throws InvalidExpressionException {
        ExpressionValidator validator = new InFixExpressionValidator();
        Expression expression2 = adjacentUnaryOperatorsExpresion();
        Assertions.assertThrows(InvalidExpressionException.class, () -> validator.validate(expression2));
    }


    @Test
    public void givenExpressionWithTwoAdjacentNumbersExceptionThrown() throws InvalidExpressionException {
        ExpressionValidator validator = new InFixExpressionValidator();
        Assertions.assertThrows(InvalidExpressionException.class, () -> validator.validate(f()));
    }

    @Test
    public void givenExpressionWithTwoAdjacentNumberAndVariableExceptionThrown() throws InvalidExpressionException {
        ExpressionValidator validator = new InFixExpressionValidator();
        Assertions.assertThrows(InvalidExpressionException.class, () -> validator.validate(h()));
    }

    @Test
    public void givenParenthesesValidExpression() throws UndeclaredVariableException, InvalidExpressionException, ExecutionException {
        ExpressionValidator validator = new InFixExpressionValidator();
        List<Token> tokens1 = new ArrayList<>();
        List<Token> tokens2 = new ArrayList<>();
        Variable a = new Variable("a");
        tokens1.add(a);
        tokens1.add(new EqualsSign());
        tokens1.add(new NumericValue<>(1));

        tokens2.add(new Variable("b"));
        tokens2.add(new EqualsSign());
        tokens2.add(a);
        tokens2.add(new IncreaseOperator(false));
        tokens2.add(new AdditionOperator());
        tokens2.add(new Open());
        tokens2.add(new NumericValue<>(5));
        tokens2.add(new MultiplicationOperator());
        tokens2.add(new NumericValue<>(5));
        tokens2.add(new Close());
        Expression expression1 = new InFixExpression(tokens1);
        Expression expression2 = new InFixExpression(tokens2);
        Assertions.assertDoesNotThrow(() -> {
            validator.validate(expression1);
        });
        Assertions.assertDoesNotThrow(() -> {
            validator.validate(expression2);
        });
    }
}
