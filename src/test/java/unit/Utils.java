package unit;

import app.entities.Expression;
import app.entities.InFixExpression;
import app.operators.IncreaseOperator;
import app.operators.NumericValue;
import app.operators.Token;
import app.operators.Variable;
import app.operators.assignment.EqualsSign;
import app.operators.basicOperators.AdditionOperator;
import app.operators.basicOperators.MultiplicationOperator;
import app.operators.parentheses.Close;
import app.operators.parentheses.Open;

import java.util.ArrayList;
import java.util.List;

public class Utils {

    public static Expression buildInvalidDoubleOperatorsExpression() {
        List<Token> list = new ArrayList<>();
        list.add(new Variable("i"));
        list.add(new AdditionOperator());
        list.add(new AdditionOperator());

        return new InFixExpression(list);
    }

    public static Expression buildValidVariablesAdditionExpression() {
        List<Token> list = new ArrayList<>();
        list.add(new Variable("i"));
        list.add(new AdditionOperator());
        list.add(new Variable("j"));
        return new InFixExpression(list);
    }

    public static Expression a() {
        List<Token> list = new ArrayList<>();
        list.add(new Open());
        list.add(new Close());

        return new InFixExpression(list);
    }

    public static Expression b() {
        List<Token> list = new ArrayList<>();
        list.add(new Close());
        list.add(new Open());

        return new InFixExpression(list);
    }

    public static Expression c() {
        List<Token> list = new ArrayList<>();
        list.add(new Variable("i"));
        list.add(new EqualsSign());
        list.add(new NumericValue<>(5));
        list.add(new AdditionOperator());
        list.add(new Variable("j"));
        list.add(new IncreaseOperator());

        return new InFixExpression(list);
    }

    public static Expression d() {
        List<Token> list = new ArrayList<>();
        list.add(new Variable("i"));
        list.add(new EqualsSign());
        list.add(new EqualsSign());
        list.add(new Variable("j"));

        return new InFixExpression(list);
    }

    public static Expression e() {
        List<Token> list = new ArrayList<>();
        list.add(new Variable("i"));
        list.add(new AdditionOperator());
        list.add(new Variable("j"));
        list.add(new AdditionOperator());
        list.add(new AdditionOperator());

        return new InFixExpression(list);
    }

    public static Expression f() {
        List<Token> list = new ArrayList<>();
        list.add(new NumericValue<>(1));
        list.add(new NumericValue<>(1));
        return new InFixExpression(list);
    }

    public static Expression h() {
        List<Token> list = new ArrayList<>();
        list.add(new NumericValue<>(1));
        list.add(new Variable("a"));
        return new InFixExpression(list);
    }

    public static Expression g() {
        List<Token> list = new ArrayList<>();
        list.add(new Variable("i"));
        list.add(new EqualsSign());
        list.add(new Variable("j"));
        list.add(new MultiplicationOperator());
        list.add(new Open());
        list.add(new NumericValue<>(5));
        list.add(new AdditionOperator());
        list.add(new NumericValue<>(3));
        list.add(new Close());

        return new InFixExpression(list);
    }

    public static Expression adjacentUnaryOperatorsExpresion() {
        List<Token> tokens2 = new ArrayList<>();
        Variable a = new Variable("a");

        tokens2.add(new Variable("b"));
        tokens2.add(new EqualsSign());
        tokens2.add(a);
        tokens2.add(new IncreaseOperator());
        tokens2.add(new IncreaseOperator());
        tokens2.add(new AdditionOperator());
        tokens2.add(new NumericValue<>(5));
        return new InFixExpression(tokens2);
    }

}
